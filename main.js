/*
todo:
 progress indication when querying balance and odds
*/
$(function () {
    // enable/disable UI according to timing
    $('.match').each(function () {
        var $m = $(this);
        var startTimestamp = Number($m.attr('data-starttimestamp'));
        var clearTimestamp = Number($m.attr('data-cleartimestamp'));

        if (startTimestamp-Date.now() > 24*24*60*60*1000) {
            return;
        }

        //initial state for "betting" is enabled
        setTimeout(function () {
            $m.find('.bet').each(function () {
                $(this).attr('disabled', 'true');
            });
        }, startTimestamp-Date.now());

        //initial state for "collecting" is disabled
        setTimeout(function () {
            $m.find('.collect').each(function () {
                $(this).removeAttr('disabled');
            });
        }, clearTimestamp-Date.now());
    });

    $('#checkbalance').click(function () {
        var x = prepare();
        var web3 = x.web3;
        var address = x.account.address;

        web3.eth.getBalance(address, function (err, balance) {
            $('#balance').val(web3.utils.fromWei(balance, 'ether')+' eth');
        });
    });


    $('.queryodds').click(function () {
        var x = prepare();
        var web3 = x.web3;
        var $tr = $(this).closest('tr');
        var contractAddr = $tr.attr('data-contract');
        var contract = new web3.eth.Contract(INTERFACE, contractAddr);
        contract.methods.valueHome().call({}, function (err, valueHome) {
            contract.methods.valueAway().call({}, function (err, valueAway) {
                $tr.find('.odds').val(
                    web3.utils.fromWei(valueHome, 'ether') + ' - ' +
                    web3.utils.fromWei(valueAway, 'ether'));
            });
        });
    });

    $('.bet').click(function () {
        var x = prepare();
        var web3 = x.web3;
        var account = x.account;
        var $tr = $(this).closest('tr');
        var contractAddr = $tr.attr('data-contract');
        var valueWei = web3.utils.toWei($tr.find('.betvalue').val(), 'ether');
        var team = $tr.find('input[type="radio"]').first().prop('checked')?1:-1;
        console.log('team:', team);
        //todo handle error event
        $('#exectx')
            .find('#close').attr('disabled', true).end()
            .find('.modal-body').each(function () { $(this).hide(); }).end()
            .find('#executing').show().end()
            .on('hide.bs.modal', function (e) {
                e.preventDefault();
            })
            .modal('show');

        execContractMethod(web3, account, contractAddr, 'bet', valueWei, [team]).then(function (r) {
            $('#exectx').find('#close').removeAttr('disabled').end()
                .find('.modal-body').each(function () { $(this).hide(); }).end()
                .find('#succeeded').show().end()
                .find('#showtx').attr('href', 'https://ropsten.etherscan.io/tx/'+r.transactionHash).end()
                .off('hide.bs.modal');
        });
    });

    $('.collect').click(function () {
        var x = prepare();
        var web3 = x.web3;
        var account = x.account;
        var $tr = $(this).closest('tr');
        var contractAddr = $tr.attr('data-contract');

        //todo handle error event
        $('#exectx')
            .find('#close').attr('disabled', true).end()
            .find('.modal-body').each(function () { $(this).hide(); }).end()
            .find('#executing').show().end()
            .on('hide.bs.modal', function (e) {
                e.preventDefault();
            })
            .modal('show');

        execContractMethod(web3, account, contractAddr, 'collect', '0', []).then(function (r) {
            $('#exectx').find('#close').removeAttr('disabled').end()
                .find('.modal-body').each(function () { $(this).hide(); }).end()
                .find('#succeeded').show().end()
                .find('#showtx').attr('href', 'https://ropsten.etherscan.io/tx/'+r.transactionHash).end()
                .off('hide.bs.modal');
        });
    });
});

// housekeeping routine
function prepare () {
    var privateKey = $('#privateKey').val();
    var provider = $('#provider').val();

    var web3 = new Web3(new Web3.providers.HttpProvider(provider));
    var account = web3.eth.accounts.privateKeyToAccount(privateKey);
    return {web3: web3, account: account};
}

// the interface for interacting with the contracts
// since the contracts only differ in constant values, they share the interface definition
var INTERFACE = [{"constant":true,"inputs":[],"name":"valueHome","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"myid","type":"bytes32"},{"name":"result","type":"string"}],"name":"__callback","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"resolve","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"myid","type":"bytes32"},{"name":"result","type":"string"},{"name":"proof","type":"bytes"}],"name":"__callback","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"team","type":"int8"}],"name":"bet","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":false,"inputs":[],"name":"getMatchResult","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"unbet","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"winner","outputs":[{"name":"","type":"int8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"collect","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"valueAway","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"anonymous":false,"inputs":[{"indexed":false,"name":"msg","type":"string"}],"name":"runtimeLog","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"scoreH","type":"uint256"},{"indexed":false,"name":"ScoreA","type":"uint256"}],"name":"debugScore","type":"event"}];

/*
Known issues:
 1.gas price returned is way too high, use hard coded value
 2.gas estimation doesn't work, use hard coded value
 3.web3.eth.accounts.signTransaction returns wrong result, have to use ethereumjs-tx
*/
function execContractMethod (web3, account, contractAddr, methodName, valueWei/*='0'*/, args/*=[]*/) {
    valueWei = valueWei || '0';
    args = args || [];

    var gasPrice = web3.utils.toWei('2', 'gwei');
    var gas = 200000;

    var contract = new web3.eth.Contract(INTERFACE, contractAddr);
    var method = contract.methods[methodName].apply(null, args);
    var data = method.encodeABI();

    return web3.eth.getTransactionCount(account.address).then(function (nonce) {
        var tx =new ethereumjs.Tx({
            nonce: web3.utils.toHex(nonce),
            gasPrice: web3.utils.toHex(gasPrice),
            gasLimit: web3.utils.toHex(gas),
            from: account.address,
            to: contract.options.address,
            value: web3.utils.toHex(valueWei),
            data: data});
        tx.sign(new ethereumjs.Buffer.Buffer(account.privateKey.slice(2), 'hex'));
        var rawTx = '0x'+tx.serialize().toString('hex');
        return web3.eth.sendSignedTransaction(rawTx);
    });
}
