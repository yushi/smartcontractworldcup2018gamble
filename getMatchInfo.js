//grab match info from fifa.com
const cheerio = require('cheerio');
const request = require('request-promise-native');

async function getInfoFromUrl (url) {
    const html = await request(url);
    const $ = cheerio.load(html);

    return { homeTeamName: $('div.t.home span.t-nText').text(),
             awayTeamName: $('div.t.away span.t-nText').text()
           };
}

async function getMatches () {
    const html = await request('http://www.fifa.com/worldcup/matches/index.html');
    //const html = require('fs').readFileSync('/tmp/fifa.html', 'utf8');
    const $ = cheerio.load(html);

    const matches = [];

    $('div.match-list-round.anchor').each(function () {
        const roundId = cheerio(this).attr('id');
        cheerio('div.match-list-date.anchor', this).each(function () {
            cheerio('div.col-xs-12.clear-grid', this).each(function () {
                const e = cheerio('div.s-score.s-date-HHmm', this);
                const m1 = e.attr('data-timeutc').match(/(\d+):(\d+)/);
                const m2 = e.attr('data-daymonthutc').match(/(\d\d)(\d\d)/);
                const timestamp = Date.UTC(2018, Number(m2[2])-1, Number(m2[1]),
                                           Number(m1[1]), Number(m1[2]));
                matches.push({roundId: roundId,
                              matchId: cheerio(this).children().first().attr('data-id'),
                              timestamp: Math.round(timestamp/1000),
                              date: String(new Date(timestamp)),
                              mmdd: m2[2]+m2[1]});
            });
        });
    });
    return matches;
}

async function main () {
    const mmdd = process.argv[2];

    const matchesP = (await getMatches())
        .filter(m=>m.mmdd===mmdd)
        .map(async function (m) {
            const {homeTeamName, awayTeamName} = await getInfoFromUrl(`http://www.fifa.com/worldcup/matches/round=${m.roundId}/match=${m.matchId}/report.html`);
            return {
                homeTeamName, awayTeamName,
                fifaRoundId: m.roundId,
                fifaMatchId: m.matchId,
                matchStartTimestamp: m.timestamp,
                matchStartTimeString: m.date
            };
        });
    console.log(JSON.stringify(await Promise.all(matchesP))); 
}

main();

//getInfoFromUrl('http://www.fifa.com/worldcup/matches/round=275073/match=300331503/report.html').then(console.log);
//getMatches().then(console.log);
