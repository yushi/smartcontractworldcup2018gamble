//script to deploy a betting contract
//node deploy.js ropsten|localhost <env file> <goal line>
//
const assert = require('assert');
const fs = require('fs');
const Web3 = require('web3');
const {encode}=require("eth-lib/lib/rlp");

const provider = 
    (process.argv[2]==='ropsten') ? new Web3.providers.HttpProvider('https://ropsten.infura.io/59HBbz2C0WXxuqmoO0ig') :
    (process.argv[2]==='localhost') ?  new Web3.providers.HttpProvider("http://localhost:8545") :
    null;
const web3 = new Web3(provider);


const mustache = require('mustache');

const mustacheSrc = fs.readFileSync('bet.sol.mustache', 'utf8');

const env = JSON.parse(fs.readFileSync(process.argv[3], 'utf8'));
//env.matchStartTimestamp = Math.round(Date.now()/1000)+60*60;
env.goalLine = process.argv[4];

const solSrc = mustache.render(mustacheSrc, env);
fs.writeFileSync('./build/bet.sol', solSrc);

const {execSync} = require('child_process');
try {
    const o = execSync('solc --combined-json bin,abi github.com/oraclize/ethereum-api=lib build/bet.sol > build/contracts.json')
} catch (e) {
    console.log('failed to compile contract');
    process.exit(e.code);
}
const contractJson = require('./build/contracts.json').contracts['build/bet.sol:Betting'];
console.log('abi:', contractJson.abi);
console.log('byte code:', contractJson.bin.slice(0, 20)+'...', contractJson.bin.length/2, 'bytes');
/* works but too slow
const solc = require('solc');

const out = solc.compile({sources: {
    'github.com/oraclize/ethereum-api/oraclizeAPI.sol': fs.readFileSync('lib/oraclizeAPI.sol', 'utf8'),
    'bet.sol': solSrc
}});

if (Object.keys(out.contracts).length===0) {
    console.log('compile error:');
    for (const e of out.errors) {
        if (!e.match(/Warning:/)) {
            console.log(e);
        }
    }
    process.exit(1);
}
//console.log(JSON.stringify(out));
*/
const account = web3.eth.accounts.decrypt(require('./ropstenWallet'), '');

var padEven = function padEven(str) {
    return str.length % 2 === 0 ? str : "0" + str;
};

async function  deploy (interf, bytecode, name, addr, privateKey, args=[] ) {
    assert(!!privateKey);
    const deploy = (new web3.eth.Contract(JSON.parse(interf))).deploy({data: '0x'+bytecode, arguments: args});
    console.log('estimating');
    const gas = await deploy.estimateGas();

    console.log('gas:', gas);
    const gasPrice = //await web3.eth.getGasPrice(); // 120 crazily high 
        web3.utils.toWei('3', 'gwei');
    console.log('gas price: %s gwei', web3.utils.fromWei(gasPrice, 'gwei')); 
    console.log('gas ether:', web3.utils.fromWei(web3.utils.toBN(gasPrice).mul(web3.utils.toBN(gas)))); 
    console.log('account balance:', web3.utils.fromWei(await web3.eth.getBalance(addr)));

    /* web3 signs incorrectly for now
      const {rawTransaction: rawTx} = await web3.eth.accounts.signTransaction({
        gas: gas, 
        data: data.bytecode}, privateKey);
    
    console.log('raw tx ', rawTx.slice(0, 100));
    */
    var Tx = require('ethereumjs-tx');

    const nonce = await web3.eth.getTransactionCount(addr); // nonce is Number

    var tx = new Tx({
        nonce: web3.utils.toHex(nonce),
        gasPrice: web3.utils.toHex(gasPrice),
        gasLimit: web3.utils.toHex(gas),
        from: addr,
        //to: '0x0000000000000000000000000000000000000000', //must not have this
        value: '0x00',
        data: '0x'+bytecode
    });
    tx.sign(new Buffer(privateKey.slice(2), 'hex'));

    var rawTx = '0x'+tx.serialize().toString('hex');
    console.log('deploying to: 0x%s', web3.utils.keccak256(encode([addr, '0x'+padEven(nonce.toString(16))])).slice(26));
    var r = await web3.eth.sendSignedTransaction(rawTx);

    return r.contractAddress;
}

deploy(contractJson.abi, contractJson.bin, 'bet.sol:Betting', account.address, account.privateKey).then(function (address) {
    console.log('deployed @', address);


    //test
    const contract = new web3.eth.Contract(JSON.parse(contractJson.abi), address);

    contract.methods.valueHome().call({from: account.address}).then(console.log);

}).catch(function (e) {
    console.log('errorrr:\n', e);
});
