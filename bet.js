// players interact with the contract through this module
const web3 = new (require('web3'));

const setProvider = exports.setProvider = function (provider) {
    web3.setProvider(provider);
};

const execContractMethod = exports.execContractMethod = async function (privateKey, contract, methodName, valueWei='0', gasPrice=null, args=[]) {
    const { address } = web3.eth.accounts.privateKeyToAccount(privateKey);
    const method = contract.methods[methodName](...args);
    const gas = 200000;
    //not working in reality: 
    //ropsten.infura.io returns totally insane numbers
    //ganache-cli explodes (VM reverts)
    //const gas = await method.estimateGas({from: '0x0000000000000000000000000000000000000000'});
    //const gas = await method.estimateGas({from: address});
    console.log('gas estimated:', gas);
    const data = method.encodeABI();

    console.log('bytecode:', data);

    /* this products an incorrect raw transaction making the VM vomit
    const {rawTransaction: rawTx} = await web3.eth.accounts.signTransaction({
        gas, data, gasPrice, value: web3.utils.toHex(valueWei)}, privateKey);
    */
    var Tx = require('ethereumjs-tx');

    const nonce = await web3.eth.getTransactionCount(address); // nonce is Number

    var tx = new Tx({
        nonce: web3.utils.toHex(nonce),
        gasPrice: web3.utils.toHex(gasPrice),
        gasLimit: web3.utils.toHex(gas),
        from: address,
        to: contract.options.address,
        value: web3.utils.toHex(valueWei),
        data: data
    });
    tx.sign(new Buffer(privateKey.slice(2), 'hex'));

    var rawTx = '0x'+tx.serialize().toString('hex');

    console.log('raw tx:', rawTx);

    return await web3.eth.sendSignedTransaction(rawTx);
};

if (require.main===module) {
    (async function () {
        const contractAddr = process.argv[2];
        const privateKey = process.argv[3];
        //setProvider(new web3.providers.HttpProvider("http://localhost:8545"));
        setProvider(new web3.providers.HttpProvider('https://ropsten.infura.io/59HBbz2C0WXxuqmoO0ig'));
        
        const contract = new web3.eth.Contract(
            JSON.parse(require('./build/contracts.json').contracts['build/bet.sol:Betting'].abi), 
            contractAddr);
        
        const {transactionHash: txHash} = await execContractMethod(privateKey, contract, 'bet', web3.utils.toWei('0.01', 'ether'), web3.utils.toWei('2', 'gwei'), [-1]);
        
        console.log('tx hash:', txHash); 
        console.log(await web3.eth.getTransactionReceipt(txHash));
        
        console.log('valueHome:', await contract.methods.valueHome().call());

        console.log('valueAway:', await contract.methods.valueAway().call());

    })().catch(function (e) {
        console.log('error calling bet:\n', e);
    });
}
