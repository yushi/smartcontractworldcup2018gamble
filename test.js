const Web3 = require('web3');
const provider = new Web3.providers.HttpProvider("http://localhost:8545");
const web3 = new Web3(provider);
const contractJson = require('./build/contracts.json').contracts['build/bet.sol:Betting'];
(async function main () {
    const accounts = await web3.eth.getAccounts();
    for (const acc of accounts) {
        const bal = await web3.eth.getBalance(acc);
        console.log('%s: %s', acc, web3.utils.fromWei(bal));
    }


    console.log('deploy %s bytes', contractJson.bin.length);
    const deploy = await (new web3.eth.Contract(JSON.parse(contractJson.abi)))
        .deploy({data: contractJson.bin});
    var gas = await deploy.estimateGas();
    console.log('gas estimated:', gas);
    const contract = await deploy
        .send({from: accounts[0], gas: gas});

    console.log('deployed at', contract.options.address);
    // Otherwise gets "Provider not set or invalid" error when calling "send" on contract methods
    // I think this a bug of web3.js
    contract.setProvider(provider);
    //console.log(contract);
/*
    console.log('__callback');
    const __callback = contract.methods.__callback('0x00', "13-1");
    gas = await __callback.estimateGas();
    console.log('gas estimated:', gas);
    var receipt = await __callback.send({from: accounts[0]});
    //console.log('receipt', receipt);


    console.log('resolve');
    await contract.methods.resolve().send({from: accounts[0]});

    const events = await contract.getPastEvents('debugScore');
    console.log('events:', events);
    
*/
    async function testResolve (rawResult) {
        await contract.methods.__callback('0x00', rawResult).send({from: accounts[0]});
        try {
            //await contract.methods.resolve().send({from: accounts[0]});
        } catch (e) {
            console.log('error when resolving', e.message);
            return;
        }
        const events = await contract.getPastEvents('debugScore');
        console.log(events[events.length-1].returnValues);
    }
/*   
    await testResolve('1-200');
    await testResolve(' 10 -  200');
    await testResolve('1:2');

    async function testWinner (rawResult) {
        await testResolve(rawResult);
        console.log('%s:\t%s', rawResult, 
                    await contract.methods.winner().call());
    }

    await testWinner('3-0');
    await testWinner('2-0');
    await testWinner('1-0');
    await testWinner('0-0');
    await testWinner('0-1');
    await testWinner('0-2');
    await testWinner('0-3');
*/
    async function estimateGasAndCall (func, options) {
        const gas = await func.estimateGas();
        console.log('gas:', gas);
        options.gas = gas*10; //fixme
        return func.send(options);
    }

    async function testBet (arr) {
        const indices = arr.map(([i])=>i);
        const orig = await Promise.all(indices.map(function (i) {
            return web3.eth.getBalance(accounts[i]);
        }));

        for ([i, team, value] of arr) {
            console.log('%s betting', i)
            await estimateGasAndCall(contract.methods.bet(team),
                                     {from: accounts[i], value: web3.utils.toWei(String(value), 'ether')});
        }

        await testResolve('3-0');

        for (i of indices) {
            console.log('%s collecting', i);
            try {
                await contract.methods.collect().send({from: accounts[i]});
            } catch (e) {
                console.log('nothing to collect');
            }
        }

        const balances = await Promise.all(indices.map(function (i) {
            return web3.eth.getBalance(accounts[i]);
        }));

        const gain = balances.map(function (b, i) {
            return web3.utils.fromWei(String(b - orig[i]), 'ether');
        });
        console.log(gain);
    }
    //testBet([[1,1, 10], [2, 1, 10], [3, 1, 10], [4, -1, 10], [5, -1, 10], [6, -1, 10]]);
    testBet([[1,1, 50], [2, 1, 50], [3, -1, 40], [4, -1, 40]]);
})().catch(function (e) {
    console.log('Error!');
    console.log(e);
});
//https://www.wikihow.com/Bet-on-Soccer
