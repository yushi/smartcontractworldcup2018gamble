SOLC = ../bin/solc
COMPILE_TARGET = build/contracts.json
SOL_SOURCE = build/bet.sol
SOL_TEMPLATE = bet.sol.mustache
ENV = devEnv.json

startClient:
	cd node_modules/ganache-cli
	- npm start --accounts 100

compile: $(COMPILE_TARGET)

$(SOL_SOURCE): $(ENV) $(SOL_TEMPLATE)
	node node_modules/mustache/bin/mustache $(ENV) $(SOL_TEMPLATE) > $@

$(COMPILE_TARGET): $(SOL_SOURCE)
	$(SOLC) --combined-json bin,abi \
	github.com/Arachnid/solidity-stringutils=lib \
	github.com/oraclize/ethereum-api=lib \
	$^ > $@

test: $(COMPILE_TARGET)
	node test.js
